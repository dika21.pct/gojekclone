import 'package:flutter/material.dart';
import '/common/my_colors.dart';
import '../widgets/tab_bar.dart';

class Promo extends StatefulWidget {
  @override
  State<Promo> createState() => _PromoState();
}

class _PromoState extends State<Promo> {
  int tabBarIndex = 1;

  var _scrollController = ScrollController();

  bool isBrush = false;

  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels > 0) {
        isBrush = true;
        setState(() {
        });
      } else {
        isBrush = false;
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: AppBar(
        backgroundColor: MyColors.green,
        title: Tab_Bar(),
      ),
      body: Stack(
        children: [
          ListView(
            controller: _scrollController,
            children: [
              SizedBox(height: 20),
              Container(
                child: Text('Ini Halaman Profile'),
              ),
              SizedBox(height: 20),
            ],
          ),
        ],
      ),
    );
  }
}